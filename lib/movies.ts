import std from "tstl";

export class Movie {
  public _title: string;
  public _actors: std.HasSet<string>;

  public constructor(title: string, actors: std.Vector<string>) {
    this._title = title;
    this._actors = actors;
  }

  public addActor(actor: string): void {
    this._actors.insert(actor);
  }

  public removeActor(actor: string): void {
    this._actors.erase(actor);
  }

  public get actors(): std.Vector<string> {
    return this._actors.toVector();
  }

  public get title(): string {
    return this._title;
  }

  public set title(title: string) {
    this._title = title;
  }
}
